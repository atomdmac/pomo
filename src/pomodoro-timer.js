const Timer = require('./timer')
const EventEmitter = require('./event-emitter')

const MS_PER_TICK = 250
const CYCLES = {
  FOCUS: {
    duration: 25,
    max: 4
  },
  SHORT_BREAK: {
    duration: 5
  },
  LONG_BREAK: {
    duration: 15
  }
}

const STATES = {
  IDLE: 'IDLE',
  FOCUS: 'FOCUS',
  SHORT_BREAK: 'SHORT_BREAK',
  LONG_BREAK: 'LONG_BREAK'
}

class PomodoroTimer {
  constructor (cyclesDefintion) {
    this.cycleDefintion = {
      ...CYCLES,
      ...cyclesDefintion
    }

    this.timer = new Timer()
    this.events.finish.addListener(this.onTimerFinish.bind(this))

    this.events = {
      stateChange: new EventEmitter(),
      restartCycle: new EventEmitter()
    }

    this.state = STATES.IDLE
    this.cycleCount = 0
    this.focusCount = 0
    this.focusCountInCycle = 0
    this.breakCount = 0
    this.breakCountInCycle = 0
  }

  pause () {
    if (this.state != STATES.IDLE) {
      this.timer.pause()
    }
  }

  unpause () {
    if (this.state != STATES.IDLE) {
      this.timer.unpause()
    }
  }

  onTimerFinish () {
    switch (this.state) {
    case STATES.FOCUS:
      if (this.focusCountInCycle === this.cyclesDefintion.FOCUS.max) {
        this.focusCountInCycle = 0
        this.startLongBreak()
      } else {
        this.focusCountInCycle++
        this.startShortBreak()
      }
      break
    case STATES.SHORT_BREAK:
      this.startFocus()
      break
    case STATES.LONG_BREAK:
      this.restartCycle()
    }
  }

  startShortBreak () {
    this.state = STATES.SHORT_BREAK
    this.timer.reset()
    this.timer.setMaxIterations(this.cycles.SHORT_BREAK.duration)
    this.events.stateChange.emit(this.state)
    this.timer.unpause()
  }

  startLongBreak () {
    this.state = STATES.LONG_BREAK
    this.timer.reset()
    this.timer.setMaxIterations(this.cycles.LONG_BREAK.duration)
    this.events.stateChange.emit(this.state)
    this.timer.unpause()
  }

  startFocus () {
    this.state = STATES.FOCUS
    this.timer.reset()
    this.timer.setMaxIterations(this.cycles.FOCUS.duration)
    this.events.stateChange.emit(this.state)
    this.timer.unpause()
  }

  restartCycle () {
    this.focusCountInCycle = 0
    this.breakCountInCycle = 0
    this.events.restartCycle.emit()
    this.startFocus()
  }
}

module.exports = {
  MS_PER_TICK,
  CYCLES,
  STATES,
  PomodoroTimer
}
