const EventEmitter = require('./event-emitter')

module.exports = class Timer {
  constructor (msInInterval = 1000, maxIterations = 60) {
    this.msInInterval = msInInterval
    this.maxIterations = maxIterations
    this.intervalId = undefined
    this.time = 0

    this.events = {
      pause: new EventEmitter(),
      unpause: new EventEmitter(),
      tick: new EventEmitter(),
      reset: new EventEmitter(),
      finish: new EventEmitter()
    }

    this.tick = this.tick.bind(this)
  }
  pause () {
    clearInterval(this.intervalId)
    this.intervalId = undefined
    this.events.pause.emit(this.time)
  }
  unpause () {
    this.intervalId = setTimeout(this.tick, this.msInInterval)
    this.events.unpause.emit(this.time)
  }
  tick () {
    this.time += 1
    this.intervalId = setTimeout(this.tick, this.msInInterval)
    if (this.time === this.maxIterations) {
      this.pause()
      this.events.finish.emit(this.time)
    } else {
      this.events.tick.emit(this.time)
    }
  }
  reset () {
    this.time = 0
    this.pause()
    this.events.reset.emit(this.time)
  }
  setMaxIterations (maxIterations) {
    this.maxIterations = maxIterations
  }
  getTime () {
    return this.time
  }
  isPaused () {
    return this.intervalId === undefined
  }
}
