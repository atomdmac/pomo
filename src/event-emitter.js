class EventEmitter {
  constructor () {
    this.listeners = []
  }
  addListener (listener) {
    this.listeners.push(listener)
  }
  removeListener (listener) {
    const index = this.listeners.indexOf(listener)
    if (index >= 0) {
      this.listeners.splice(index, 1)
    }
  }
  emit (data) {
    this.listeners.forEach(listener => listener(data))
  }
}

module.exports = EventEmitter
