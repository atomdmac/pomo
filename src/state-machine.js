const STATE_DEFAULTS = {
  name: 'State Name',
  canTransitionTo: [
    'States', 'that', 'I', 'can', 'transition', 'to'
  ],
  onEnter: () => {},
  onExit: () => {}
}

class StateMachine {
  constuctor () {
    this.currentState = null
    this.states = {}
  }
  addState (state) {
    // TODO: Make sure state object shape is correct
    const newState = {
      ...STATE_DEFAULTS,
      ...state
    }
    if (!this.states[newState.name]) {
      this.states[newState.name] = newState
    }
  }
  removeState (stateName) {
    if (this.currentState && this.currentState.name != stateName) {
      delete this.states[stateName]
    }
  }
  transition (newStateName) {
    if (!this.states[newStateName]) {
      throw new Error(`No state with the name ${newStateName} currently exists.`)
    }
    const canTransition = this.currentState.canTransitionTo.some(
      possibleTransitionName => possibleTransitionName === newStateName
    )
    if (!canTransition) {
      return false
    } else {
      this.currentState.onExit()
      this.currentState = this.states[newStateName]
      this.currentState.onEnter()
      return true
    }
  }
}

module.exports = StateMachine
