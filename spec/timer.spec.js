'use strict'
const Timer = require('../src/timer')

jest.useFakeTimers()

describe('timer', () => {
  describe('constructor', () => {
    test('should set internal state', () => {
      const timer = new Timer()
      expect(timer.msInInterval).toBe(1000)
      expect(timer.maxIterations).toBe(60)
      expect(timer.intervalId).toBe(undefined)
      expect(timer.time).toBe(0)
    })
  })
  describe('unpause', () => {
    test('should not move timer forward if not called', () => {
      jest.useFakeTimers()
      const milliseconds = 1000
      const iterations = 60
      const timer = new Timer(milliseconds, iterations)
      const tickSpy = jest.spyOn(timer, 'tick')

      jest.advanceTimersByTime(milliseconds * iterations)
      expect(tickSpy).not.toBeCalled()

      tickSpy.mockRestore()
    })
    test('should allow time to advance forward', () => {
      jest.useFakeTimers()
      const milliseconds = 1000
      const iterations = 60
      const timer = new Timer(milliseconds, iterations)
      const tickSpy = jest.spyOn(timer, 'tick')

      timer.unpause()
      jest.advanceTimersByTime(milliseconds * iterations)
      expect(tickSpy).toBeCalledTimes(iterations)

      tickSpy.mockRestore()
    })
  })
  describe('pause', () => {
    test('should stop time when called', () => {
      jest.useFakeTimers()
      const milliseconds = 1000
      const iterations = 2
      const timer = new Timer(milliseconds, iterations)
      const tickSpy = jest.spyOn(timer, 'tick')

      timer.unpause()
      jest.advanceTimersByTime(milliseconds * (iterations / 2))
      expect(tickSpy).toBeCalledTimes(1)
      timer.pause()
      jest.advanceTimersByTime(milliseconds * (iterations / 2))
      expect(tickSpy).toBeCalledTimes(1)

      tickSpy.mockRestore()
    })
  })
  describe('events', () => {
    test('unpause/pause', () => {
      jest.useFakeTimers()
      const milliseconds = 1000
      const iterations = 60
      const timer = new Timer(milliseconds, iterations)
      const pauseListener = jest.fn()
      const unpauseListener = jest.fn()
      timer.events.pause.addListener(pauseListener)
      timer.events.unpause.addListener(unpauseListener)
      timer.unpause()
      expect(unpauseListener).toBeCalledTimes(1)
      expect(pauseListener).not.toBeCalled()
      timer.pause()
      expect(pauseListener).toBeCalledTimes(1)
    })
    describe('tick', () => {
      test('should emit `tick` event for each iteration', () => {
        jest.useFakeTimers()
        const milliseconds = 1000
        const iterations = 60
        const timer = new Timer(milliseconds, iterations)
        const tickListener = jest.fn()
        timer.events.tick.addListener(tickListener)
        timer.unpause()
        jest.advanceTimersByTime(milliseconds * iterations)
        expect(tickListener).toBeCalledTimes(iterations - 1)
        // Ensure `tick` is called w/ correct time values.
        tickListener.mock.calls.forEach((args, callIndex) => {
          expect(args).toEqual([callIndex + 1])
        })
      })
      test('should emit `finish` event after last iteration', () => {
        jest.useFakeTimers()
        const milliseconds = 1000
        const iterations = 60
        const timer = new Timer(milliseconds, iterations)
        const finishListener = jest.fn()
        timer.events.finish.addListener(finishListener)
        timer.unpause()
        jest.advanceTimersByTime(milliseconds * iterations)
        expect(finishListener).toBeCalledTimes(1)
      })
    })
    test('pause', () => {
      jest.useFakeTimers()
      const milliseconds = 1000
      const iterations = 60
      const timer = new Timer(milliseconds, iterations)
      const listener = jest.fn()
      timer.events.unpause.addListener(listener)
      timer.unpause()
      expect(listener).toBeCalledTimes(1)
    })
  })
})
